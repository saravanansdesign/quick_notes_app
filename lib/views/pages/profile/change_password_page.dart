import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {

  UtilService utils = UtilService();

  bool _resetBtnEnabled = true;
  String _resetBtnTxt = "Update Password";
  TextEditingController newPass = TextEditingController();
  TextEditingController confirmPass = TextEditingController();
  bool newObscure = true;
  bool confirmObscure = true;

  var userFullName = "";
  var userEmail = "";
  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Change Password',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: utils.hexColor("#ffffff"),
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
              width: 1,
              color: utils.hexColor("#dddddd"),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Change Your password", style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,fontSize: 22)),
              Padding(
                padding: const EdgeInsets.only(top: 10,bottom: 35),
                child: Text("Your new password must be diffenrent \n from previous used passwords.",
                  style: TextStyle(fontSize: 17,height:1.2,color: Colors.black54),),
              ),
              TextFormField(
                controller: newPass,
                keyboardType : TextInputType.visiblePassword,
                obscureText: newObscure,
                decoration:  InputDecoration(
                  labelText: 'New Password',
                  border: OutlineInputBorder(),
                  suffixIcon: IconButton(
                    icon: Icon(
                      newObscure ? Icons.visibility_off : Icons.visibility,
                    ), onPressed: () {
                    setState(() {
                      newObscure = !newObscure;
                    });
                  },
                  ),
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Enter your new password';
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top:35),
                child: TextFormField(
                  controller: confirmPass,
                  keyboardType : TextInputType.visiblePassword,
                  obscureText: confirmObscure,
                  decoration:  InputDecoration(
                    labelText: 'Confirm Password',
                    border: OutlineInputBorder(),
                    suffixIcon: IconButton(
                      icon: Icon(
                        confirmObscure ? Icons.visibility_off : Icons.visibility,
                      ), onPressed: () {
                      setState(() {
                        confirmObscure = !confirmObscure;
                      });
                    },
                    ),
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter confirm password';
                    }
                    return null;
                  },
                ),
              ),
              const Text("Must be at least 8 characters", style: TextStyle(height:1.5,color: Colors.black54),),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width -55,
                      child: ElevatedButton.icon(
                          onPressed: _resetBtnEnabled ? (){
                            if(newPass.text == ""){
                              utils.fToast("New Password is required","bottom");

                            }else if(confirmPass.text == ""){
                              utils.fToast("Confirm Password is required","bottom");

                            }else if(newPass.text != confirmPass.text) {
                              utils.fToast("Password does not matched!","bottom");

                            }else if(newPass.text != confirmPass.text){
                              setState(() {
                                _resetBtnEnabled  = false;
                                _resetBtnTxt  = "Loading...";
                              });

                              EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
                              // loginCtrl.userLogin(emailCtrl.text, pwdCtrl.text).then((result) async {
                              EasyLoading.dismiss();
                              setState(() {
                                _resetBtnEnabled = true;
                                _resetBtnTxt = "Update Password";
                              });
                              // });
                            }
                          } : null,
                          label: Text(_resetBtnTxt),
                          icon: const Icon(Icons.arrow_forward),
                          style: ElevatedButton.styleFrom(
                            primary: AppTheme.primary,
                            textStyle: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w800,
                              fontFamily: "Play",
                              fontSize: 16,
                            ),
                          )
                      ),
                    )
                  ],
                ),
              ),





            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
  }
}
