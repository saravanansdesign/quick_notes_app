import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:quick_notes_app/app-conf.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
class AppInfoPage extends StatefulWidget {
  @override
  _AppInfoPageState createState() => _AppInfoPageState();
}

class _AppInfoPageState extends State<AppInfoPage> {

  final primaryColor = const Color(0xFFF59726);
  final bgColor = const Color(0xFFFFFFFF);
  var userFullName = "";
  var userEmail = "";
  Map? userObj;
  UtilService utils = new UtilService();
  final LocalStorage storage2 = new LocalStorage('dash');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#f0f0f0"),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('App Info',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: Flex(
          direction: Axis.vertical,
          mainAxisAlignment: MainAxisAlignment.center,
          children :  [
            SizedBox(
              height: 350.0,
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(bottom: 10.0),
                      child: Image.asset(
                        'assets/images/logo.png',
                        height: 90.0,
                      )),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text("BOOK SHUTTLE".toUpperCase(), style: TextStyle(color : utils.hexColor("#363636"),fontSize: 22.0,fontWeight: FontWeight.w600,letterSpacing: 1.0),)
                      )
                  ),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Text("A Visitor Management App", style: TextStyle(color : utils.hexColor("#464646"),fontSize: 18.0,),)
                      )
                  ),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.only(bottom: 5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text.rich(
                                TextSpan(
                                  text: "App Version ${Env.conf["app_version"]}",
                                  style: TextStyle(fontSize: 14, color: Colors.blueGrey),
                                ),
                              )
                            ],
                          )
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                            child: InkWell(
                                child: Text('License', style: TextStyle(color : utils.hexColor("#666666"),fontSize: 16.0,fontWeight: FontWeight.w600,decoration: TextDecoration.underline),),
                                onTap: () => utils.openBrowserTab("https://boodskap.io/pricing")
                            )
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Center(
                            child: Text('|', style: TextStyle(color : utils.hexColor("#aaaaaa"),fontSize: 16.0,fontWeight: FontWeight.w600),),
                          ),),
                        Center(
                            child: InkWell(
                                child: Text('About Us', style: TextStyle(color : utils.hexColor("#666666"),fontSize: 16.0,fontWeight: FontWeight.w600,decoration: TextDecoration.underline),),
                                onTap: () => utils.openBrowserTab("https://boodskap.io/company")
                            )
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ]
      ),
    );
  }

  Future<void> openBrowserTab(String link) async {
    await FlutterWebBrowser.openWebPage(url: link);
  }

  @override
  void initState() {
    userObj = storage2.getItem('userObj') ?? null;
  }
}
