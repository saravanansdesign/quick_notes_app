import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {

  UtilService utils = UtilService();

  var userFullName = "";
  var userEmail = "";
  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Notifications',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [],
          )
        ],
      ),
    );
  }

  @override
  void initState() {
  }
}
