import 'package:flutter/material.dart';
import 'package:quick_notes_app/controllers/shuttle_controller.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';
import 'package:quick_notes_app/routes/app_pages.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class SearchResultPage extends StatefulWidget {
  const SearchResultPage({Key? key}) : super(key: key);

  @override
  _SearchResultPageState createState() => _SearchResultPageState();
}

class _SearchResultPageState extends State<SearchResultPage> {

  UtilService utils = UtilService();
  ShuttleController shuttleCtrl = ShuttleController();

  var searchObj;
  List searchResult = [];
  List shuttleList = [];
  Map? userObj;
  Map? selectedShuttle;
  String? searchLocation;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: AppTheme.primary),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Icon(FontAwesomeIcons.bus, color: AppTheme.primary,),
            ),
            Text("Available Shuttles", style: TextStyle(color: utils.hexColor("#363636")),)
          ],
        ),
        backgroundColor: AppTheme.lightBackground,
        bottom: PreferredSize(
            child: Container(
              color: AppTheme.secondary,
              height: 4.0,
            ),
            preferredSize: const Size.fromHeight(4.0)),
      ),
      body: SingleChildScrollView(
        child:  Container(
          margin: const EdgeInsets.all(0.0),
          padding: const EdgeInsets.all(0.0),
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          color: utils.hexColor("#d6d6d6"),
          child: _renderLocations(),
      ),
      ),
    );
  }

  Widget _renderLocations(){
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: shuttleList.length,
      itemBuilder: (build, index) {
        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width-10,
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: utils.hexColor("#f6f6f6"),
                border: Border(
                    bottom: BorderSide(
                        color: utils.hexColor("#dddddd"),
                        width: 1.0
                    )
                )
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:5.0),
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 5.0),
                        child: FaIcon(FontAwesomeIcons.mapMarkerAlt, size: 16.0, color: AppTheme.primary,),
                      ),
                      Text("${utils.getCap(shuttleList[index]['from_loc'])}", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: AppTheme.secondary),),
                      Text(" to ", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500, color: utils.hexColor("#666666")),),
                      Text("${utils.getCap(shuttleList[index]['to_loc'])}", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: AppTheme.primary),),
                      const Spacer(),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            FaIcon(FontAwesomeIcons.rupeeSign, size: 16.0, color: AppTheme.primary),
                            Padding(padding: EdgeInsets.only(left: 3.0), child: Text("50", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w700, color: AppTheme.primary),),),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top:7.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center ,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: FaIcon(FontAwesomeIcons.bus, size: 16.0),
                      ),
                      // ignore: prefer_const_constructors
                      Text("${utils.getCap(shuttleList[index]['shuttle_name'])} - ", style: TextStyle(fontSize: 16.0),),
                      Text("${utils.getCap(shuttleList[index]['curr_loc_name'])}", style: TextStyle(fontSize: 16.0),),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:7.0),
                  child: Row(
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(right: 5.0),
                        child: FaIcon(FontAwesomeIcons.clock, size: 16.0,),
                      ),
                      const Text("Travel : ", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),),
                      Text("${utils.getCap(shuttleList[index]['trip_start_time'])}", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, color: utils.hexColor("#363636")),),
                      const Text(" to ", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),),
                      Text("${utils.getCap(shuttleList[index]['trip_end_time'])} ", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, color: utils.hexColor("#363636")),),
                      const Spacer(),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            // FaIcon(FontAwesomeIcons.chair, size: 16.0, color: AppTheme.primary),
                            Image.asset("assets/images/seats.png", height: 20.0,),
                            Padding(padding: EdgeInsets.only(left: 3.0), child: Text("40", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700, color: AppTheme.primary),),),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          onTap: (){
            selectedShuttle = shuttleList[index];
            Get.toNamed(AppPages.singleShuttleBooking, arguments: selectedShuttle);
          },
        );
      },
    );
  }

  void getShuttleList() async{
    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);
    var res = await shuttleCtrl.getShuttleList(searchObj);
    EasyLoading.dismiss();

    if(res["status"]){
       setState(() {
         shuttleList = res["result"];
       });
     }else{
       setState(() {
         shuttleList = [];
       });
     }
  }

  @override
  void initState() {
    searchObj = Get.arguments;
    getShuttleList();
  }
}
