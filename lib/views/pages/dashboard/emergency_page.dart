import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';

class EmergencyPage extends StatefulWidget {
  const EmergencyPage({Key? key}) : super(key: key);

  @override
  _EmergencyPageState createState() => _EmergencyPageState();
}

class _EmergencyPageState extends State<EmergencyPage> {

  UtilService utils = UtilService();

  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title:  Text('Emergency',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font, color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: SingleChildScrollView(

        child:  Container(
          margin: EdgeInsets.only(top:20,left:20),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: utils.hexColor("#cccccc"),
                      blurRadius: 2.0,
                      spreadRadius: 3.0,
                      // offset: const Offset(0, 2),
                    )
                  ],
                ),
                child: InkWell(
                  onTap: (){},
                  child: CircleAvatar(
                    backgroundColor: AppTheme.primary,
                    radius: 50,
                    child: ClipOval(
                      child: FaIcon(FontAwesomeIcons.fire,color: Colors.white, size:50),
                    ),
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top:5),
                  child: Text("Tap in case of emergency",style: TextStyle(fontSize: 10,color: Colors.black54),)),

              Container(
                // height: 300,
                child: GridView.count(
                  shrinkWrap: true,
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 2,
                  children: <Widget>[
                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),
                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),

                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),
                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),

                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),
                    InkWell(
                      onTap: (){},
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: utils.hexColor("#cccccc"),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(25),
                        child: Column(
                          children: [
                            FaIcon(FontAwesomeIcons.radiation, color: AppTheme.primary,size: 50,),
                            Text("Radiation",style: TextStyle(fontSize:20,fontFamily: AppTheme.font,fontWeight: FontWeight.bold,color: Colors.black54),),
                          ],
                        ),

                        // color: Colors.teal[100],
                      ),
                    ),

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
  }
}
