import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';

class LocationSearchPage extends StatefulWidget {
  const LocationSearchPage({Key? key}) : super(key: key);

  @override
  _LocationSearchPageState createState() => _LocationSearchPageState();
}

class _LocationSearchPageState extends State<LocationSearchPage> {

  UtilService utils = UtilService();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var locationObj;
  List searchResult = [];
  dynamic locationsList = ConstVar.locationsList;
  Map? userObj;
  String? searchLocation;
  TextEditingController locationSearchCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: utils.hexColor("#363636")),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: TextField(
          controller: locationSearchCtrl,
          obscureText: false,
          decoration: InputDecoration(
            hintText: "Search Your Location",
            hintStyle: TextStyle(fontSize: 18, color: utils.hexColor("#aaaaaa")),
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            contentPadding: const EdgeInsets.only(left: 0, bottom: 11, top: 11, right: 15),
          ),
        ),
        backgroundColor: AppTheme.lightBackground,
      ),
      body: SingleChildScrollView(
        child:  Container(
          margin: EdgeInsets.all(0.0),
          padding: EdgeInsets.all(0.0),
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          color: utils.hexColor("#d6d6d6"),
          child: _renderLocations(),
      ),
      ),
    );
  }

  Widget _renderLocations(){
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: locationsList.length,
      itemBuilder: (build, index) {
        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width-10,
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: utils.hexColor("#f6f6f6"),
                border: Border(
                    bottom: BorderSide(
                        color: utils.hexColor("#dddddd"),
                        width: 1.0
                    )
                )
            ),
            child: Text(locationsList[index]["location_name"], style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),),
          ),
          onTap: (){
            locationObj = locationsList[index];
            if(utils.isNull(locationObj)){
              locationSearchCtrl.text = locationObj["location_name"];
            }
            Get.back(result: locationsList[index]);
          },
        );
      },
    );
  }

  @override
  void initState() {
    locationObj = Get.arguments;
    print("locationObj----------");
    print(locationObj);
    if(utils.isNull(locationObj)){
      locationSearchCtrl.text = locationObj["location_name"];
    }
  }
}
