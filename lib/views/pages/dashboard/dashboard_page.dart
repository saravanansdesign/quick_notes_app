import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/routes/app_pages.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:quick_notes_app/app-conf.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:quick_notes_app/controllers/dashboard_controller.dart';
import 'package:quick_notes_app/views/pages/dashboard/notepad_page.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:quick_notes_app/modules/utils/dhz_times.dart';
import 'package:html_editor_enhanced/html_editor.dart';

var clientSessionId = "WEB_${DateTime.now().millisecondsSinceEpoch}";
final client =
    MqttServerClient('wss://${Env.conf["MQTT_BASE"]}', clientSessionId);

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  Dhs dhs = Dhs();
  UtilService utils = UtilService();
  AuthApiService authService = AuthApiService();
  DashboardController dashCtrl = DashboardController();
  TextEditingController fromLocationCtrl = TextEditingController();
  TextEditingController toLocationCtrl = TextEditingController();
  String result = '';
  final HtmlEditorController controller = HtmlEditorController();

  bool connStatus = false;
  bool is_dashboard_open = true;
  bool branding_dashboard = false;

  var userObj;
  var dashObj;
  var userEmail = "";
  var mqttStatus = "Connecting...";
  var customerObj;
  var userFullName = "";
  var SELECTED_DASH;
  var mqttStatusBarBg = const Color(0xFFFDD734);
  var LOGO_BRANDING_URL = Image.asset('assets/images/boodskap-logo.png', height: 24.0);
  var defaultImg = const AssetImage('assets/images/boodskap-logo.png');
  var profileImgSet = Image.asset(
    'assets/images/user.png',
    fit: BoxFit.cover,
  );

  dynamic fromLocation = {
    "_id": "",
    "location_name": "From Location",
    "lat": "",
    "long": ""
  };
  dynamic toLocation = {
    "_id": "",
    "location_name": "To Location",
    "lat": "",
    "long": ""
  };
  dynamic bookingDate = Text("Today",
      style: TextStyle(
          fontFamily: AppTheme.font,
          fontSize: 16.0,
          fontWeight: FontWeight.w800,
          color: UtilService().hexColor("#363636")));
  dynamic plansCard = ConstVar.plansCard;
  dynamic promotionCards = ConstVar.promotionCards;
  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  int _selectedIndex = 0;

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return await showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: const Text('Are you sure?'),
                content: const Text('Do you want to exit an App'),
                actions: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: const Padding(
                        padding: EdgeInsets.all(10.0), child: Text("No")),
                  ),
                  const SizedBox(height: 16),
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(true),
                    child: const Padding(
                        padding: EdgeInsets.all(10.0), child: Text("Yes")),
                  ),
                ],
              ),
            ) ??
            false;
      },
      child: Scaffold(
        key: _key,
        backgroundColor: utils.hexColor("#eeeeee"),
        body: SingleChildScrollView(
          child: Column(
            children: [
              if(_selectedIndex == 0)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 45,
                    ),
                    Padding(padding: EdgeInsets.only(left: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Image.asset(
                                  "assets/images/logo.png",
                                  height: 24.0),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "MY QUICK NOTES",
                              style: TextStyle(
                                fontFamily: AppTheme.font,
                                color: AppTheme.secondary,
                                fontSize: 18,
                                fontWeight: FontWeight.w900,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Expanded(child: Row()),
                          Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: TextButton(
                              style: TextButton.styleFrom(
                                  backgroundColor: AppTheme.secondary),
                              onPressed: () async {
                                saveMyNote();
                              },
                              child: Text(
                                'Save',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),),
                    HtmlEditor(
                      controller: controller,
                      htmlEditorOptions: HtmlEditorOptions(
                        hint: 'Your text here...',
                        shouldEnsureVisible: true,
                        //initialText: "<p>text content initial, if any</p>",
                      ),
                      htmlToolbarOptions: HtmlToolbarOptions(
                        toolbarPosition: ToolbarPosition.aboveEditor,
                        //by default
                        toolbarType: ToolbarType.nativeScrollable,
                        //by default
                        onButtonPressed:
                            (ButtonType type, bool? status, Function? updateStatus) {
                          print(
                              "button pressed, the current selected status is $status");
                          return true;
                        },
                        onDropdownChanged: (DropdownType type, dynamic changed,
                            Function(dynamic)? updateSelectedItem) {
                          print(
                              "dropdown changed to $changed");
                          return true;
                        },
                        mediaLinkInsertInterceptor:
                            (String url, InsertFileType type) {
                          print(url);
                          return true;
                        },
                      ),
                      otherOptions: OtherOptions(height: MediaQuery.of(context).size.height - 180),
                      callbacks: Callbacks(onBeforeCommand: (String? currentHtml) {
                        print('html before change is $currentHtml');
                      }, onChangeContent: (String? changed) {
                        print('content changed to $changed');
                      }, onChangeCodeview: (String? changed) {
                        print('code changed to $changed');
                      }, onChangeSelection: (EditorSettings settings) {
                        print('parent element is ${settings.parentElement}');
                        print('font name is ${settings.fontName}');
                      }, onDialogShown: () {
                        print('dialog shown');
                      }, onEnter: () {
                        print('enter/return pressed');
                      }, onFocus: () {
                        print('editor focused');
                      }, onBlur: () {
                        print('editor unfocused');
                      }, onBlurCodeview: () {
                        print('codeview either focused or unfocused');
                      }, onInit: () {
                        print('init');
                      },
                          onImageUploadError: (FileUpload? file, String? base64Str,
                              UploadError error) {
                            print(base64Str ?? '');
                            if (file != null) {
                              print(file.name);
                              print(file.size);
                              print(file.type);
                            }
                          }, onKeyDown: (int? keyCode) {
                            print('$keyCode key downed');
                            print(
                                'current character count: ${controller.characterCount}');
                          }, onKeyUp: (int? keyCode) {
                            print('$keyCode key released');
                          }, onMouseDown: () {
                            print('mouse downed');
                          }, onMouseUp: () {
                            print('mouse released');
                          }, onNavigationRequestMobile: (String url) {
                            print(url);
                            return NavigationActionPolicy.ALLOW;
                          }, onPaste: () {
                            print('pasted into editor');
                          }, onScroll: () {
                            print('editor scrolled');
                          }),
                      plugins: [
                        SummernoteAtMention(
                            getSuggestionsMobile: (String value) {
                              var mentions = <String>['test1', 'test2', 'test3'];
                              return mentions
                                  .where((element) => element.contains(value))
                                  .toList();
                            },
                            mentionsWeb: ['test1', 'test2', 'test3'],
                            onSelect: (String value) {
                              print(value);
                            }),
                      ],
                    ),
                  ],
                )
              else if(_selectedIndex == 1)
                Text(
                  'Index 1: Home',
                  style: optionStyle,
                )
              else if(_selectedIndex == 2)
                Text(
                  'Index 2: Home',
                  style: optionStyle,
                )
              ,
              // _renderAllNotesWidget()
            ],
          ),
        ),
        drawer: Theme(
            data: Theme.of(context).copyWith(
              canvasColor: utils.hexColor("#ffffff"),
            ),
            child: Drawer(
              child: Stack(children: <Widget>[
                ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.zero,
                  children: [
                    SizedBox(
                      height: 120.0,
                      child: GestureDetector(
                          child: DrawerHeader(
                              decoration: BoxDecoration(
                                  color: AppTheme.primary,
                                  border: Border(
                                      bottom: BorderSide(
                                          color: utils.hexColor("#ffffff"),
                                          width: 5.0))),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: FittedBox(
                                      fit: BoxFit.contain,
                                      // otherwise the logo will be tiny
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          border: Border.all(
                                            width: 2.0,
                                            color: utils.hexColor("#ffffff"),
                                          ),
                                        ),
                                        child: Wrap(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              child: CircleAvatar(
                                                  backgroundColor:
                                                      Colors.white30,
                                                  child: profileImgSet),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 8,
                                      child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0, top: 5.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                customerObj != null
                                                    ? utils.getCap(
                                                        "${customerObj["firstname"]} ${customerObj["lastname"]}")
                                                    : "-",
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 18.0,fontFamily: AppTheme.font,
                                                    fontWeight: FontWeight.bold,
                                                    color: utils
                                                        .hexColor("#ffffff")),
                                              ),
                                              Text(
                                                  (customerObj != null)
                                                      ? customerObj["email"]
                                                      : "-",
                                                  textAlign: TextAlign.left,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(fontFamily: AppTheme.font,
                                                      color: utils.hexColor(
                                                          "#ffffff"))),
                                            ],
                                          )))
                                ],
                              )),
                          onTap: () {
                            Get.toNamed(AppPages.profile);
                          }),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text(
                            'Dashboard',
                            style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646")),
                          ),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.th,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Dashboard',
                          ),
                          onTap: () {
                            Get.back();
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('My Profile',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.userCircle,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Profile',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.profile);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Settings',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.cog,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Profile',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.settings);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Help & Support',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.lifeRing,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Help & Support',
                          ),
                          onTap: () {
                            Get.toNamed(AppPages.helpAndSupport);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                          color: utils.hexColor("#ffffff"),
                          border: Border(
                            bottom: BorderSide(
                                width: 1.0, color: utils.hexColor("#dddddd")),
                          ),
                        ),
                        child: ListTile(
                          title: Text('Logout',
                              style: TextStyle(fontFamily: AppTheme.font,
                                fontSize: 16.0,
                                color: utils.hexColor("#464646"),
                              )),
                          minLeadingWidth: 16,
                          leading: Icon(
                            FontAwesomeIcons.signOutAlt,
                            color: AppTheme.primary,
                            size: 16.0,
                            semanticLabel: 'Logout',
                          ),
                          onTap: () async {
                            await authService.logout();
                            Get.offNamed(AppPages.login);
                          },
                        )),
                  ],
                ),
                Positioned(
                  left: 0.0,
                  bottom: 0.0,
                  child: Container(
                      width: MediaQuery.of(context).size.width - 85,
                      decoration: BoxDecoration(
                        color: utils.hexColor("#ffffff"),
                        border: Border(
                          bottom: BorderSide(
                              width: 1.0, color: utils.hexColor("#ffffff")),
                        ),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(child: LOGO_BRANDING_URL))),
                ),
              ]),
            )),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.notes),
              label: 'My Notes',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add),
              label: 'Create',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.restore_from_trash),
              label: 'Trash',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: AppTheme.primary,
          backgroundColor: AppTheme.secondary,
          unselectedItemColor: AppTheme.lightBackground,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void saveMyNote() async{
    var txt = await controller.getText();
    if (txt.contains('src=\"data:')) {
      txt =
      '<text removed due to base-64 data, displaying the text could cause the app to crash>';
    }
    setState(() {
      result = txt;
    });

    print("saveMyNote-----------------");
    print(result);
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _notepadWidgets() => Row(
    children: [
      Text("Notepad Page"),
    ],
  );

  Widget _headerWidget() => SizedBox(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                            padding: EdgeInsets.only(top: 7, left: 10.0, right: 10.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(bottom: 18.0),
                                  child: Row(
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 10.0),
                                          child: Image.asset(
                                              "assets/images/logo.png",
                                              height: 24.0),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "MY QUICK NOTES",
                                          style: TextStyle(
                                            fontFamily: AppTheme.font,
                                            color: AppTheme.secondary,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w900,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget _renderAllNotesWidget() => Container(
      margin: const EdgeInsets.symmetric(vertical: 7),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
      itemCount: plansCard.length,
      itemBuilder: (b, i) {
        return  _folderWidget();
      })
  );

  Widget _folderWidget() => Column(
    children: [
      Padding(
          padding: EdgeInsets.only(top: 10.0, left: 15.0),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "My Recent Trips",
                style: TextStyle(fontFamily: AppTheme.font,
                    color: utils.hexColor("#363636"),
                    fontSize: 16.0,
                    fontWeight: FontWeight.w800),
              ))),
      Container(
        margin: const EdgeInsets.symmetric(vertical: 7),
        padding: const EdgeInsets.symmetric(horizontal: 10),
        height: 126,
        child: _noteWidget(),
      )
    ],
  );

  Widget _noteWidget() => ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: plansCard.length,
      itemBuilder: (b, i) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          padding: const EdgeInsets.all(15.0),
          height: 120,
          decoration: BoxDecoration(
            color: utils.hexColor(plansCard[i]["plan_color"]),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: const EdgeInsets.only(left: 3, right: 3),
                  child: Image.asset(
                    "assets/images/${plansCard[i]["icon"]}",
                    height: 40.0,
                    fit: BoxFit.cover,
                  )),
              Padding(
                  padding: const EdgeInsets.only(
                      top: 5, left: 3, right: 3),
                  child: Text(
                    "${plansCard[i]["plan_name"]}",
                    style: TextStyle(fontFamily: AppTheme.font,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: utils
                            .hexColor(plansCard[i]["text_color"])),
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  )),
              Padding(
                padding: const EdgeInsets.only(
                    top: 2, left: 3, right: 3, bottom: 3),
                child: Text(
                  "${plansCard[i]["desc"]}",
                  style: TextStyle(fontFamily: AppTheme.font,
                      fontSize: 13,
                      color:
                      utils.hexColor(plansCard[i]["text_color"])),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        );
      });

  void getUserProfileImg() async {
    setState(() {
      Env.conf["user_pro_img_id"] = customerObj["hphoto"];
      profileImgSet = Image.network(
        '${Env.conf["api_base_path"]}/files/download/${Env.conf["api_token"]}/${Env.conf["user_pro_img_id"]}?${DateTime.now().millisecondsSinceEpoch}',
        height: 60.0,
        width: 60.0,
        fit: BoxFit.cover,
      );
    });
  }

  @override
  void initState() {
    super.initState();
  }
}