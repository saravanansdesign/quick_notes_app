import 'package:flutter/services.dart';
import 'package:quick_notes_app/routes/app_pages.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:quick_notes_app/controllers/login_controller.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../../app-conf.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isChecked = false;
  bool onchange = false;
  bool reset = false;

  LoginController loginCtrl = LoginController();
  AuthApiService authApiService = AuthApiService();

  TextEditingController emailCtrl =  TextEditingController();
  TextEditingController passwordCtrl =  TextEditingController();
  TextEditingController primaryPhoneCtrl =  TextEditingController();
  TextEditingController firstNameCtrl =  TextEditingController();
  TextEditingController lastNameCtrl =  TextEditingController();
  // TextEditingController countryCtrl =  TextEditingController();
  // TextEditingController stateCtrl =  TextEditingController();
  // TextEditingController cityCtrl =  TextEditingController();
  // TextEditingController addressCtrl =  TextEditingController();
  // TextEditingController zipcodeCtrl =  TextEditingController();
  // TextEditingController localeCtrl =  TextEditingController();
  // TextEditingController timezoneCtrl =  TextEditingController();

  bool _signupBtnEnabled = true;
  String _loginBtnTxt = "Confirm, Proceed";
  UtilService utils = UtilService();

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      return AppTheme.outLineBorder;
    }
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 1.0),
                        child: Image.asset('assets/images/logo.png',height: 90.0,fit: BoxFit.cover,)
                    ),
                     Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 10.0),
                        child: Text.rich(
                          TextSpan(
                            text: 'Book Shuttle',
                            style: TextStyle(fontSize: 18, color: utils.hexColor("#464646"), fontWeight: FontWeight.w600),
                          ),
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
                        child: Text.rich(
                          TextSpan(
                            text: 'Sign Up',
                            style: TextStyle(fontSize: 20, color: utils.hexColor("#363636")),
                          ),
                        )),

                    Padding(
                        padding: EdgeInsets.all(7.0),
                        child: SizedBox(
                          // height: 50.0,
                            width: MediaQuery.of(context).size.width - 30,
                            child: TextFormField(
                                controller: firstNameCtrl,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Enter Your First Name';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'First Name',
                                ),
                                onChanged: (val) {
                                  if(onchange){
                                    if (_formKey.currentState!.validate()) {}
                                  }
                                }))),
                    Padding(
                        padding: EdgeInsets.all(7.0),
                        child: SizedBox(
                          // height: 50.0,
                            width: MediaQuery.of(context).size.width - 30,
                            child: TextFormField(
                                controller: lastNameCtrl,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Enter Your First Name';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Last Name',
                                ),
                                onChanged: (val) {
                                  if(onchange){
                                    if (_formKey.currentState!.validate()) {}
                                  }
                                }))),
                    Padding(
                        padding: EdgeInsets.all(7.0),
                        child: SizedBox(
                            // height: 50.0,
                            width: MediaQuery.of(context).size.width - 30,
                            child: TextFormField(
                                controller: emailCtrl,
                                obscureText: false,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                      return 'Enter you email id';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Email ID',
                                ),
                                onChanged: (val) {
                                  if(onchange){
                                    if (_formKey.currentState!.validate()) {}
                                  }
                                }))),
                    Padding(
                        padding: EdgeInsets.all(7.0),
                        child: SizedBox(
                            // height: 50.0,
                            width: MediaQuery.of(context).size.width - 30,
                            child: TextFormField(
                                controller: passwordCtrl,
                                obscureText: true,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Enter you password';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Password',
                                ),
                                onChanged: (val) {
                                    if(onchange){
                                    if (_formKey.currentState!.validate()) {}
                                    }
                                }))),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 7.0),
                      child:  TextFormField(
                        controller: primaryPhoneCtrl,
                        maxLength: 10,
                        inputFormatters: [ FilteringTextInputFormatter.allow(RegExp("[0-9]"))],
                        decoration: const InputDecoration(
                          labelText: 'Mobile Number *',
                          contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                          border: OutlineInputBorder(),
                          counterText: "",
                        ),
                        keyboardType : TextInputType. number,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            if(reset){
                              return null;
                            }else{
                              return 'Enter your mobile number';
                            }
                          } else  if (value.length != 10){
                            return 'Mobile number must be in 10 digit';
                          }
                          return null;
                        },

                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 50.0,
                            width: MediaQuery.of(context).size.width - 30,
                            child: ElevatedButton.icon(
                                onPressed: _signupBtnEnabled ? (){
                                  if(firstNameCtrl.text == ""){
                                    utils.fToast("First Name is required","bottom");
                                  }
                                  else if(lastNameCtrl.text == ""){
                                    utils.fToast("Last Name is required","bottom");
                                  }
                                  else if(emailCtrl.text == ""){
                                    utils.fToast("Email ID is required","bottom");
                                  }
                                  else if(passwordCtrl.text == ""){
                                    utils.fToast("Password is required","bottom");
                                  }

                                  onchange = true;
                                  if (_formKey.currentState!.validate()) {

                                   if(_signupBtnEnabled){
                                    setState(() {
                                      _signupBtnEnabled  = false;
                                      _loginBtnTxt  = "Loading...";
                                    });

                                    EasyLoading.show(status: 'loading...',maskType: EasyLoadingMaskType.black);

                                    var userObj = {
                                      "email": emailCtrl.text,
                                      "password": passwordCtrl.text,
                                      "firstName": firstNameCtrl.text,
                                      "lastName": lastNameCtrl.text,
                                    };

                                      loginCtrl.userSignUp(userObj).then((result) async {
                                        EasyLoading.dismiss();

                                        setState(() {
                                          _signupBtnEnabled = true;
                                          _loginBtnTxt = "Confirm, Proceed";
                                        });

                                        if (result["status"]) {
                                          utils.fToast("Registration Completed Successfully!", "bottom");
                                          Get.offNamed(AppPages.login);
                                        } else {
                                          utils.fToast("${result["data"]}","bottom");
                                        }
                                      });
                                     }
                                  }
                                } : null,
                                label: Text(_loginBtnTxt),
                                icon: FaIcon(FontAwesomeIcons.check, size: 18, color:AppTheme.solidBtnText),
                                style: ElevatedButton.styleFrom(
                                  primary: AppTheme.primary,
                                  textStyle: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 16,
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: 30.0, bottom: 5.0),
                        child : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text('Powered by ',style: TextStyle(fontSize: 12, color: Colors.blueGrey),),
                            Image.asset('assets/images/boodskap-logo.png',height: 20.0,)
                          ],
                        )),
                  ],
                ),
              )
          )),
    );
  }

  @override
  void initState(){
  }
}
