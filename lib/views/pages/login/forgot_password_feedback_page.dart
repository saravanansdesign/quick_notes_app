import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';

class ForgotPasswordFeedbackPage extends StatefulWidget {
  const ForgotPasswordFeedbackPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordFeedbackPageState createState() => _ForgotPasswordFeedbackPageState();
}

class _ForgotPasswordFeedbackPageState extends State<ForgotPasswordFeedbackPage> {

  UtilService utils = UtilService();

  Map? userObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title:  Text('ForgotPasswordFeedback',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top:100,bottom: 20),
                padding: EdgeInsets.all(20),
                decoration:BoxDecoration(
                  color:utils.hexColor("#ffffff"),
                  // color: Colors.blue,
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                child: Icon(FontAwesomeIcons.envelopeOpenText,color: AppTheme.primary, size:55)),
            Text("Check your mail",style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,fontSize: 25)),
            Padding(
              padding: const EdgeInsets.only(top:15),
              child: Text("We have sent a password recover \n instructions to your mail",style: TextStyle(fontSize: 15,height:1.2,color: Colors.black54),textAlign: TextAlign.center),
            ),


            Container(
              margin: EdgeInsets.fromLTRB(0,30,0,35),
              child: MaterialButton(
                  height: 50.0,
                  elevation: 10,
                  // minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: Theme.of(context).primaryColor),
                  ),
                  child: Text(
                    "Open email app",
                    style: TextStyle(color: Colors.white, fontFamily: 'Play'),
                  ),
                  color: AppTheme.primary,
                  onPressed: () {}
              ),
            ),
            InkWell(
              // When the user taps the button, show a snackbar.
              onTap: () {

              },
              child: const Padding(
                padding: EdgeInsets.all(12.0),
                child: Text("Skip, I'll confirm later"),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
  }
}
