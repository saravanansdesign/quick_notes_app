import 'package:flutter/material.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:quick_notes_app/modules/utils/constant_variables.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:quick_notes_app/controllers/login_controller.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  UtilService utils = UtilService();
  AuthApiService authApiService = AuthApiService();
  LoginController loginCtrl = LoginController();

  Map? userObj;
  bool _resetBtnEnabled = true;
  String _resetBtnTxt = "Forgot Password";
  TextEditingController emailAddress = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: utils.hexColor("#e7f5ff"),
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title:  Text('Forgot Password',
          style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,color: Colors.white)),
          backgroundColor: AppTheme.primary,
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: utils.hexColor("#ffffff"),
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              width: 1,
              color: utils.hexColor("#dddddd"),
            ),
          ),
          padding: EdgeInsets.all(16.0),
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Forgot password",style: TextStyle(fontWeight: FontWeight.bold,fontFamily: AppTheme.font,fontSize: 26)),
              Padding(
                padding: const EdgeInsets.only(top: 10,bottom: 35),
                child: Text("Enter the email associated with your account \n and we'll send an email with instructions to \n reset your password",
                  style: TextStyle(fontSize: 15,height:1.2,color: Colors.black54),),
              ),
              TextFormField(
                controller: emailAddress,
                keyboardType : TextInputType.emailAddress,
                decoration:  InputDecoration(
                  labelText: 'Email address',
                  border: OutlineInputBorder(),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 50.0,
                      child: ElevatedButton.icon(
                          onPressed: _resetBtnEnabled ? (){
                            if(emailAddress.text == ""){
                              utils.fToast("Email address is required","bottom");
                            }else if(_resetBtnEnabled){
                              setState(() {
                                _resetBtnEnabled  = false;
                                _resetBtnTxt  = "Loading...";
                              });
                              forgotPassword(context);
                            }
                          } : null,
                          label: Text(_resetBtnTxt),
                          icon: const Icon(Icons.arrow_forward),
                          style: ElevatedButton.styleFrom(
                            primary: AppTheme.primary,
                            textStyle: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w800,
                              fontSize: 16,
                            ),
                          )
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> forgotPassword(context) async {
    EasyLoading.show(status: "Loading...");
      var res = await loginCtrl.forgotPasswordCtrl(emailAddress.text);
      EasyLoading.dismiss();
      setState(() {
        _resetBtnEnabled  = true;
        _resetBtnTxt  = "Forgot Password";
        emailAddress.text = "";
      });

      if(res["status"]){
        utils.sweetAlert("success", context, "Reset Password Link Sent", "Check your mail inbox, click the reset link to change your password");
      }else{
        utils.sweetAlert("error", context, "Email ID Not Exists", "Oops! Given email address is not associated with any one of our accounts");
      }
  }

  @override
  void initState() {
  }
}
