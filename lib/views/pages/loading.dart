import 'package:flutter/material.dart' show BuildContext, Center, Key, Row, StatelessWidget, Text, Widget;

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: const [
          Text("Loading..."),
        ],
      ),
    );
  }
}
