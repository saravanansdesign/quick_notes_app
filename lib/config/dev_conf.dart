class DevEnvironment {
  static Map config = {
    "app_version" : "1.0.0",
    "platform_version" : "",
    "api_version" : "",
    "api_token" : "",
    "api_base_path" : "",
    "mqtt_base" : "",
    "domain_key" : "",
    "api_key" : "",
    "user_pro_img_id" : ""
  };
}