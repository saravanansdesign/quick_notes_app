import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:localstorage/localstorage.dart';
import 'package:quick_notes_app/app-conf.dart';
import 'package:dio/dio.dart';
import 'package:mime/mime.dart';
import 'package:get/get.dart';
import 'package:quick_notes_app/modules/utils/log.dart';
import 'package:get_storage/get_storage.dart';

class CommonApiService {
  var userObj = "".obs;
  final LocalStorage storage2 = LocalStorage('dash');

  // Is Already Logged In
  Future<bool> isLoggedIn() async {
    bool flag = false;
    await storage2.ready;
    if (await storage2.getItem('userObj') != null &&
        await storage2.getItem('userObj') != "") {
      flag = true;
    }
    return flag;
  }

  Future loginAPI(username, password) async {

    var inputObj = {'email': username, 'password': password};

    final url = "${Env.conf["api_base_path"]}/domain/login";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    log(url);
    log(inputObj);
    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(inputObj));
    var resultObj = {
      "status": false,
      "result": {},
    };
    var resData = json.decode(response.body);

    if (response.statusCode == 200) {
      resultObj["status"] = true;
      Env.conf["api_token"] = resData["token"];

      if (resData["partDomains"].length == 1) {
        var res = await getDomainDetailsAPI(resData["partDomains"][0]["domainKey"]);
        resultObj["multi"] = false;
        resultObj["status"] = res["status"];
        resultObj["result"] = res["result"];

      } else if (resData["partDomains"].length > 1) {
        resultObj["multi"] = true;
        resultObj["result"] = resData;
        // await storage2.setItem('userObj', resData);
      }
    } else {
      resultObj = {
        "status": false,
        "result": resData,
      };
      // await storage2.setItem('userObj', null);
    }

    return resultObj;
  }

  Future getDomainDetailsAPI(domainKey) async {

    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url = "${Env.conf["api_base_path"]}/domain/login/switch/${Env.conf["api_token"]}/$domainKey";

    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      Env.conf["api_token"] = jsonData["token"];
      Env.conf["domain_key"] = jsonData["domainKey"];
      Env.conf["api_key"] = jsonData["apiKey"];
      resultObj = {
        "status": true,
        "result": jsonData,
      };
    } else {
      resultObj = {
        "status": false,
        "result": jsonData,
      };
      await storage2.setItem('userObj', null);
    }

    return resultObj;
  }

  Future getVersion() async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url = "${Env.conf["api_base_path"]}/cluster/statistics";

    final response = await http.get(Uri.parse(url), headers: headers);
    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      Env.conf["platform_version"] = jsonData["version"];
      resultObj = {
        "status": true,
        "result": jsonData,
      };
    } else {
      resultObj = {
        "status": false,
        "result": jsonData,
      };
    }

    return resultObj;
  }

  Future getUserProperty(email) async {
    Map<String, String> headers = {'Content-Type': 'application/json'};
    var url =
        "${Env.conf["api_base_path"]}/user/property/get/${Env.conf["api_token"]}/$email/user.picture";

    final response = await http.get(Uri.parse(url), headers: headers);
    var jsonData = json.decode(response.body);
    var resultObj = {};

    if (response.statusCode == 200) {
      resultObj = {
        "status": true,
        "result": json.decode(jsonData["value"])["picture"],
      };
    } else {
      resultObj = {
        "status": false,
        "result": null,
      };
    }

    return resultObj;
  }

  Future updateProfilePicture(username, password) async {
    final url =
        "${Env.conf["api_base_path"]}/user/property/upsert/${Env.conf["api_token"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.post(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);
    storage2.setItem('userObj', jsonData);
    return jsonData;
  }

  Future userProfileUpdate(inputObj) async {
    final url =
        "${Env.conf["api_base_path"]}/user/upsert/${Env.conf["api_token"]}";
    Map<String, String> headers = {'Content-Type': 'application/json'};

    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(inputObj));

    var jsonData = json.decode(response.body);
    return jsonData;
  }

  Future getUserProfile(email) async {
    final url =
        "${Env.conf["api_base_path"]}/user/get/${Env.conf["api_token"]}/${email}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getDashboardList(email) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/domain.mobile.dashboard.list";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var result = json.decode(response.body);
    var jsonData = {};

    if (result!.length > 0) {
      jsonData["result"] = json.decode(response.body);
      jsonData["status"] = true;
    } else {
      jsonData["message"] = json.decode(response.body);
      jsonData["status"] = false;
    }

    return jsonData;
  }

  Future getDomainProperty(name) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${name}";
    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);
    var resultObj = {};
    var jsonData = json.decode(response.body);

    if (response.statusCode == 200) {
      resultObj["status"] = true;
      resultObj["result"] = jsonData;
    } else {
      resultObj["status"] = false;
      resultObj["result"] = jsonData;
    }

    return resultObj;
  }

  Future getSingleDashboardWidgetsAPI(dash_property) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${dash_property}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getSingleWidgetAPI(dash_property) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/${dash_property}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  Future getWidgetsList(dashboard_id) async {
    final url =
        "${Env.conf["api_base_path"]}/domain/property/get/${Env.conf["api_token"]}/mobile.${dashboard_id}";

    Map<String, String> headers = {'Content-Type': 'application/json'};
    final response = await http.get(Uri.parse(url), headers: headers);

    var jsonData = json.decode(response.body);

    return jsonData;
  }

  // Logout
  Future<void> logout() async {
    // Simulate a future for response after 1 second.
    // storage2.getItem('userObj') = "";
    // storage2.clear();
    // return true;
  }

  void fToast(msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void initState() {
    String? userObjRes = GetStorage().read<String>('userObj');
    userObj = userObjRes as RxString;
  }
}
