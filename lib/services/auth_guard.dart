import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';
import 'package:quick_notes_app/routes/app_pages.dart';
import 'package:quick_notes_app/modules/utils/log.dart';

class AuthMiddleware extends GetMiddleware {

  @override
  int? get priority => 1;

  @override
  Future<GetNavConfig?> redirectDelegate(GetNavConfig route) async {
    var _prefs = await SharedPreferences.getInstance();
    var userObj = _prefs.getString("userObj");

    log("!!!!!!!!!!userObj!!!!!!!!!!");
    log(userObj);

    if (userObj != null) {
      if(route.location == "/login" || route.location == "/"){
        return GetNavConfig.fromRoute(AppPages.dashboard);
      }else{
        return await super.redirectDelegate(route);
      }
    }else{
      return GetNavConfig.fromRoute(AppPages.login);
    }
  }
}
