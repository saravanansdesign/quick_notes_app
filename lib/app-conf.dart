import 'package:quick_notes_app/config/dev_conf.dart';
import 'package:quick_notes_app/config/test_conf.dart';
import 'package:quick_notes_app/config/prod_conf.dart';

class Env {

  // static Map conf = DevEnvironment.config;
  static Map conf = TestEnvironment.config;
// static Map conf = ProdEnvironment.config;
}