import 'dart:convert';
import 'package:get/get.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:quick_notes_app/views/pages/login/login_page.dart';
import '../app-conf.dart';

class LoginController extends GetxController {

  AuthApiService authService = AuthApiService();
  LoginPage loginForm = LoginPage();

  userLogin(String username, String password) async {

    var responseObj = {};
    var response1 = await authService.loginAPI(username, password);
    responseObj["userRes"] = response1;
    var isHostAccess = false;

    if(response1["status"]){
      for(var i=0; i<response1["result"]["user"]["roles"].length; i++){
        if(response1["result"]["user"]["roles"][i] == "customer"){
          isHostAccess = true;
        }
      }

      var customerMobile = "";

      var query = {
        "query": {
          "bool": {
            "must": [
              {
                "match": {"domainKey": Env.conf["domain_key"]}
              },
              {
                "match": {"mobile": customerMobile}
              }
            ]
          }
        },
        "size": 1,
        "sort": []
      };

      var queryObj = {
        "method": "GET",
        "extraPath": "",
        "query": jsonEncode(query),
        "params": []
      };

      var response2 = await authService.getCustomerDetailsAPI(queryObj);
      responseObj["customerRes"] = response2;
    }

    return responseObj;
  }

  userSignUp(dynamic userObj) async {

    var responseObj = {};
    var isUserExist = await authService.isUserExistAPI(userObj["email"]);

    if(!isUserExist){

      var inputObj = {
        "domainKey": Env.conf["domain_key"],
        "email": userObj["email"],
        "password": userObj["password"],
        "firstName": userObj["firstName"],
        "lastName": userObj["lastName"],
        "primaryPhone": "",
        "country": "IN",
        "state": "",
        "city": "",
        "address": "",
        "zipcode": "",
        "locale": "EN",
        "timezone": "",
        "workStart": 0,
        "workEnd": 0,
        "workDays": [0],
        "roles": ["customer"],
        "groups": [0],
        "registeredStamp": 0
      };

      var userResult = await authService.platformUserCreateAPI(inputObj);

      if(userResult["status"]){

        var customerObj = {
          "address1" : "",
          "address2" : "",
          "city" : "",
          "created_by" : userObj["email"],
          "created_time" : 0,
          "customer_no" : "TEXT",
          "email" : userObj["email"],
          "firstname" : userObj["firstName"],
          "lastname" : userObj["lastName"],
          "home_loc_lat" : "",
          "home_loc_long" : "",
          "is_active" : "not-verified",
          "last_updt_by" : "",
          "last_updt_time" : 0,
          "mobile" : "",
          "office_address" : "",
          "office_loc_lat" : "",
          "office_loc_long" : "",
          "pincode" : "",
          "state" : ""
        };

        var customerResult = await authService.customerCreateAPI(customerObj);
        responseObj["status"] = true;
        responseObj["data"] = customerResult;

      }else{
        responseObj["status"] = false;
        responseObj["data"] = "Something went wrong, User sign up failed!";
      }
    }else{
      responseObj["status"] = false;
      responseObj["data"] = "Email ID is Already Registered";
    }

    return responseObj;
  }

  forgotPasswordCtrl(String email) async {

    var inputObj = {
      "sessionId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "namedrule": "forgot-password",
      "scriptArgs": {
        "email":email
      }
    };

    var response = await authService.forgotPasswordAPI(inputObj);
    return response;
  }
}