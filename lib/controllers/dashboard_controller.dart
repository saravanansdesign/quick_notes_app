import 'dart:convert';
import 'package:get/get.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:quick_notes_app/services/dashboard_api.dart';
import 'package:quick_notes_app/modules/utils/log.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:quick_notes_app/app-conf.dart';

class DashboardController extends GetxController {

  AuthApiService authApiService = AuthApiService();
  UtilService utils = UtilService();
  DashboardApiService dashboardApiService = DashboardApiService();

}