import 'dart:convert';
import 'package:get/get.dart';
import 'package:quick_notes_app/modules/utils/common.dart';
import 'package:quick_notes_app/services/auth_api.dart';
import 'package:quick_notes_app/services/settings_api.dart';
import 'package:quick_notes_app/modules/utils/log.dart';
import 'package:quick_notes_app/app-conf.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class SettingsController extends GetxController {

  SettingsApiService settingsApiService = SettingsApiService();
  AuthApiService authApiService = AuthApiService();
  UtilService utils = UtilService();
  var userObj;
  var customerObj;

  updateHostNotificationCtrl(inputObj) async{

    userObj = await authApiService.getUserObj();
    customerObj = await authApiService.getCustomerObj();
    var oneHost = customerObj;
    oneHost["updatedby"] = customerObj["hemail"];
    oneHost["updatedtime"] = utils.dhzMillis();
    oneHost["is_push_enabled"] = inputObj["pushToggle"];
    oneHost["is_email_enabled"] = inputObj["mailToggle"];
    oneHost["is_sms_enabled"] = inputObj["smsToggle"];

    var _id = await oneHost.remove("_id");
    var resObj = await settingsApiService.updateHostNotificationAPI(oneHost, _id);
    return resObj;
  }

  getCustomerDetails() async {

    userObj = await authApiService.getUserObj();
    var customerMobile = "";

    var query = {
      "query": {
        "bool": {
          "must": [
            {
              "match": {"domainKey": Env.conf["domain_key"]}
            },
            {
              "match": {"mobile": customerMobile}
            }
          ]
        }
      },
      "size": 1,
      "sort": []
    };

    print("query-----------------");
    print(query);

    var queryObj = {
      "method": "GET",
      "extraPath": "",
      "query": jsonEncode(query),
      "params": []
    };

    var response = await authApiService.getCustomerDetailsAPI(queryObj);

    print("response-------------------");
    print(response);

    var singleHostObj;

    if(utils.isNull(response["result"]["data"])){
      if(response["result"]["data"]["data"].length > 0){
        singleHostObj = response["result"]["data"]["data"][0];
        if(singleHostObj != null){
          SharedPreferences.getInstance().then((prefs) {
            var _save = json.encode(singleHostObj);
            log("customerObj=> $_save");
            prefs.setString("customerObj", _save);
          });
        }
      }
    }

    return response;
  }
}