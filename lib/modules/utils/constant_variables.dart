import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quick_notes_app/routes/app_pages.dart';
import 'common.dart';

class AppTheme {

  static var font = "Nunito";

  // General Theme Colors
  static var primary = UtilService().hexColor("#F4CC26"); //Primary Color
  static var primaryDark = UtilService().hexColor("#f9b42d"); //Primary Color
  static var secondary = UtilService().hexColor("#252C39"); //Secondary Color
  static var fadeSecondary = UtilService().hexColor("#F9F2D7"); //Secondary Color
  static var lightBackground = UtilService().hexColor("#ffffff"); //Secondary Color
  static var greyBackground = UtilService().hexColor("#eeeeee"); //Secondary Color
  static var greyText = UtilService().hexColor("#aaaaaa"); //Secondary Color

  // Outline Button
  static var outLineText = UtilService().hexColor("#F4CC26");
  static var outLineBg = UtilService().hexColor("#ffffff");
  static var outLineBorder = UtilService().hexColor("#F4CC26");

  // Solid Button
  static var solidBtnText = UtilService().hexColor("#ffffff");
  static var solidBtnBg = UtilService().hexColor("#F4CC26");
  static var solidBtnBorder = UtilService().hexColor("#F4CC26");
}

class AppTable {
  static var shuttleMasterTable = 10000; //Shuttle Master Table
  static var shuttleStatusTable = 10001; //Shuttle Status Table
  static var driverMasterTable = 10002; //Driver Master Table
  static var seatMasterTable = 10003; //Seat Master Table
  static var bookingStatusTable = 10004; //Booking Status Table
  static var paymentStatusTable = 10005; //Payment Status Table
  static var transactionHistoryTable = 10006; //Transaction History Table
  static var customerMasterTable = 10007; //Customer Master Table
  static var tripMasterTable = 10008; //Trip Master Table
  static var notepadTable = 10009; //Trip History Table
}


class ConstVar {

  static dynamic locationsList = [
    {
      "_id" : "1",
      "location_name" : "Porur",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "2",
      "location_name" : "Valasaravakkam",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "3",
      "location_name" : "Virugambakkam",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Saligramam",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Vadapalani",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "KK Nagar",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Ashok Pillar",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Ekkattuthangal",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Guindy",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Asargana",
      "lat" : 0,
      "long" : 0,
    },
    {
      "_id" : "1",
      "location_name" : "Meenambakkam",
      "lat" : 0,
      "long" : 0,
    }
  ];

  static dynamic plansCard = [{
    "plan_name" : "Silver Plan",
    "plan_color" : "#ffffff",
    "text_color" : "#363636",
    "plan_type" : "silver",
    "desc" : "Plan for often shuttle using users",
    "icon" : "silver-badge.png",
    "price" : "1200",
    "validity_months" : "1",
    "currency" : "100"
  }, {
    "plan_name" : "Gold Plan",
    "plan_type" : "gold",
    "plan_color" : "#fcd670",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "gold-badge.png",
    "price" : "2400",
    "validity_months" : "4",
    "currency" : "100"
  }, {
    "plan_name" : "Diamond Plan",
    "plan_type" : "diamond",
    "plan_color" : "#89c4f4",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "diamond-badge.png",
    "price" : "3600",
    "validity_months" : "8",
    "currency" : "100"
  }, {
    "plan_name" : "Platinum Plan",
    "plan_type" : "platinum",
    "plan_color" : "#d5b8ff",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "platinum-badge.png",
    "price" : "4800",
    "validity_months" : "12",
    "currency" : "100"
  }];


  static dynamic promotionCards = [{
    "plan_name" : "Silver Plan",
    "plan_color" : "#ffffff",
    "text_color" : "#363636",
    "plan_type" : "silver",
    "desc" : "Plan for often shuttle using users",
    "icon" : "silver-badge.png",
    "price" : "1200",
    "validity_months" : "1",
    "currency" : "100"
  }, {
    "plan_name" : "Gold Plan",
    "plan_type" : "gold",
    "plan_color" : "#fcd670",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "gold-badge.png",
    "price" : "2400",
    "validity_months" : "4",
    "currency" : "100"
  }, {
    "plan_name" : "Diamond Plan",
    "plan_type" : "diamond",
    "plan_color" : "#89c4f4",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "diamond-badge.png",
    "price" : "3600",
    "validity_months" : "8",
    "currency" : "100"
  }, {
    "plan_name" : "Platinum Plan",
    "plan_type" : "platinum",
    "plan_color" : "#d5b8ff",
    "text_color" : "#ffffff",
    "desc" : "Plan for often shuttle using users",
    "icon" : "platinum-badge.png",
    "price" : "4800",
    "validity_months" : "12",
    "currency" : "100"
  }];
}