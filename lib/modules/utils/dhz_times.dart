import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:intl/intl.dart';

class Dhs {

  @override
  void initState() {
  }

  today(){
    var now = DateTime.now();
    String today = DateFormat('dd/MMM/yyyy').format(now);
    return today;
  }

  dhzMillis(){
    return DateTime.now().millisecondsSinceEpoch;
  }

  dhzTStoDate(timestamp){

    if(timestamp != "" && timestamp != null){
      timestamp = timestamp;

      var dt = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var d12 = DateFormat('dd/MM/yyyy').format(dt); // 12/31/2000, 10:00 PM
      return d12;
    }else{
      return null;
    }
  }

  dhzTStoDateTime(timestamp){

    if(timestamp != "" && timestamp != null){
      timestamp = timestamp;

      var dt = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var d12 = DateFormat('dd/MM/yyyy hh:m a').format(dt); // 12/31/2000, 10:00 PM
      return d12;
    }else{
      return null;
    }
  }
}
