import 'package:quick_notes_app/modules/bindings/auth_binding.dart';
import 'package:quick_notes_app/views/pages/dashboard/notepad_page.dart';
import 'package:quick_notes_app/views/pages/dashboard/shuttle_search_page.dart';
import 'package:quick_notes_app/views/pages/dashboard/single_shuttle_booking_page.dart';
import 'package:quick_notes_app/views/pages/dashboard/trip_history_page.dart';
import 'package:quick_notes_app/views/pages/login/forgot_password_feedback_page.dart';
import 'package:quick_notes_app/views/pages/login/forgot_password_page.dart';
import 'package:quick_notes_app/views/pages/login/login_page.dart';
import 'package:quick_notes_app/views/pages/dashboard/dashboard_page.dart';
import 'package:quick_notes_app/views/pages/login/signup_page.dart';
import 'package:quick_notes_app/views/pages/profile/app_info_page.dart';
import 'package:quick_notes_app/views/pages/profile/change_password_page.dart';
import 'package:quick_notes_app/views/pages/profile/help_and_support_page.dart';
import 'package:quick_notes_app/views/pages/profile/notifications_page.dart';
import 'package:quick_notes_app/views/pages/profile/preferences_page.dart';
import 'package:quick_notes_app/views/pages/profile/profile_page.dart';
import 'package:quick_notes_app/views/pages/profile/settings_page.dart';
import 'package:quick_notes_app/views/pages/page_not_found.dart';
import 'package:quick_notes_app/services/auth_guard.dart';
import 'package:quick_notes_app/views/pages/dashboard/location_search_page.dart';

import 'package:get/get.dart';
part 'app_routes.dart';

class AppPages {
  AppPages._();

  //Default Pages
  static const initial = Routes.LOADING;
  static const login = Routes.LOGIN;
  static const signup = Routes.SIGNUP;
  static const forgotPassword = Routes.FORGOT_PASSWORD;
  static const forgotPasswordFeedback = Routes.FORGOT_PASSWORD_FEEDBACK;
  static const dashboard = Routes.DASHBOARD;
  static const profile = Routes.PROFILE;
  static const helpAndSupport = Routes.HELP_AND_SUPPORT;
  static const appInfo = Routes.APP_INFO;
  static const settings = Routes.SETTINGS;
  static const notifications = Routes.NOTIFICATIONS;
  static const preferences = Routes.PREFERENCES;
  static const changePassword = Routes.CHANGE_PASSWORD;

  //Application Pages
  static const locationSearch = Routes.LOCATION_SEARCH;
  static const searchResult = Routes.SEARCH_RESULT;
  static const notepad = Routes.NOTEPAD;
  static const singleShuttleBooking = Routes.SINGLE_SHUTTLE_BOOKING;

  static final unknownRoute =
  GetPage(name: '/pagenotfound', page: () => const UnknownRoutePage());

  // static String LOGIN_THEN(String afterSuccessfulLogin) => '$login?then=${Uri.encodeQueryComponent(afterSuccessfulLogin)}';
  static String LOGIN_THEN(String afterSuccessfulLogin) => '$login';

  static final List<GetPage<dynamic>> routes = <GetPage<dynamic>>[
    GetPage(
      name: login,
      page: () => LoginPage(),
      title: 'Log In',
      middlewares: [AuthMiddleware()],
      binding: AuthBinding(),
    ),
    GetPage(
      name: signup,
      page: () => SignUpPage(),
      title: 'Log In',
      middlewares: [AuthMiddleware()],
      binding: AuthBinding(),
    ),
    GetPage(
      name: forgotPassword,
      page: () => ForgotPasswordPage(),
      title: 'Forgot Password',
      middlewares: [AuthMiddleware()],
      binding: AuthBinding(),
    ),
    GetPage(
      name: forgotPasswordFeedback,
      page: () => ForgotPasswordFeedbackPage(),
      title: 'Feedback',
      middlewares: [AuthMiddleware()],
      binding: AuthBinding(),
    ),
    GetPage(
      name: dashboard,
      page: () => DashboardPage(),
      title: 'Dashboard',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: searchResult,
      page: () => SearchResultPage(),
      title: 'Search Result',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: notepad,
      page: () => NotepadPage(),
      title: 'Notepad',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: singleShuttleBooking,
      page: () => SingleShuttleBookingPage(),
      title: 'Selected Single Shuttle View',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: profile,
      page: () => ProfilePage(),
      title: 'Profile',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: appInfo,
      page: () => AppInfoPage(),
      title: 'App Info',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: helpAndSupport,
      page: () => HelpAndSupportPage(),
      title: 'Help & Support',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: settings,
      page: () => SettingsPage(),
      title: 'Settings',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: notifications,
      page: () => NotificationsPage(),
      title: 'Notifications',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: preferences,
      page: () => PreferencesPage(),
      title: 'Preferences',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: changePassword,
      page: () => ChangePasswordPage(),
      title: 'Change Password',
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: notepad,
      page: () => NotepadPage(),
      title: 'Notepad',
      middlewares: [AuthMiddleware()],
    ),
  ];
}
